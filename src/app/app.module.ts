import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { ClienteComponent } from './clientes/cliente/cliente.component';
import { ClienteFormComponent } from './clientes/cliente-form/cliente-form.component';
import { MenuComponent } from './menu/menu.component';
import { InstituicaoEnsinoComponent } from './clientes/academico/instituicao-ensino/instituicao-ensino.component';
import { SistemaComponent } from './clientes/sistemico/sistema/sistema.component';
import { ModuloComponent } from './clientes/sistemico/modulo/modulo.component';
import { FuncionalidadeComponent } from './clientes/sistemico/funcionalidade/funcionalidade.component';
import { SistemaFormComponent } from './clientes/sistemico/sistema-form/sistema-form.component';
import { ModuloFormComponent } from './clientes/sistemico/modulo-form/modulo-form.component';
import { FuncionalidadeFormComponent } from './clientes/sistemico/funcionalidade-form/funcionalidade-form.component';
import { AreaEstudoComponent } from './clientes/academico/area-estudo/area-estudo.component';
import { InstituicaoEnsinoFormComponent } from './clientes/academico/instituicao-ensino-form/instituicao-ensino-form.component';
import { AreaEstudoFormComponent } from './clientes/academico/area-estudo-form/area-estudo-form.component';
import { EscolaridadeComponent } from './clientes/academico/escolaridade/escolaridade.component';
import { EscolaridadeFormComponent } from './clientes/academico/escolaridade-form/escolaridade-form.component';
import { AtividadadeEscolarComponent } from './clientes/academico/atividadade-escolar/atividadade-escolar.component';
import { AtividadadeEscolarFormComponent } from './clientes/academico/atividadade-escolar-form/atividadade-escolar-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SemRegistrosComponent } from './utils/tabelas/sem-registros/sem-registros.component';


@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    ClienteFormComponent,
    MenuComponent,
    InstituicaoEnsinoComponent,
    SistemaComponent,
    ModuloComponent,
    FuncionalidadeComponent,
    SistemaFormComponent,
    ModuloFormComponent,
    FuncionalidadeFormComponent,
    AreaEstudoComponent,
    InstituicaoEnsinoFormComponent,
    AreaEstudoFormComponent,
    EscolaridadeComponent,
    EscolaridadeFormComponent,
    AtividadadeEscolarComponent,
    AtividadadeEscolarFormComponent,
    SemRegistrosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [],
  entryComponents : [
    ClienteFormComponent,
    SistemaFormComponent,
    ModuloFormComponent,
    FuncionalidadeFormComponent,
    AtividadadeEscolarFormComponent,
    AreaEstudoFormComponent,
    InstituicaoEnsinoFormComponent,
    EscolaridadeFormComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
