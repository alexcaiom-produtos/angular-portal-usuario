import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemRegistrosComponent } from './sem-registros.component';

describe('SemRegistrosComponent', () => {
  let component: SemRegistrosComponent;
  let fixture: ComponentFixture<SemRegistrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemRegistrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemRegistrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
