import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreaEstudoComponent } from './clientes/academico/area-estudo/area-estudo.component';
import { AtividadadeEscolarComponent } from './clientes/academico/atividadade-escolar/atividadade-escolar.component';
import { EscolaridadeComponent } from './clientes/academico/escolaridade/escolaridade.component';
import { InstituicaoEnsinoComponent } from './clientes/academico/instituicao-ensino/instituicao-ensino.component';
import { ClienteComponent } from './clientes/cliente/cliente.component';
import { FuncionalidadeComponent } from './clientes/sistemico/funcionalidade/funcionalidade.component';
import { ModuloComponent } from './clientes/sistemico/modulo/modulo.component';
import { SistemaComponent } from './clientes/sistemico/sistema/sistema.component';


const routes: Routes = [
  { path : '', component: ClienteComponent },
  { path : 'sistema', component: SistemaComponent},
  { path : 'modulo', component: ModuloComponent},
  { path : 'funcionalidade', component: FuncionalidadeComponent},
  { path : 'academico/atividade-escolar', component: AtividadadeEscolarComponent},
  { path : 'academico/area-estudo', component: AreaEstudoComponent},
  { path : 'academico/escolaridade', component: EscolaridadeComponent},
  { path : 'academico/instituicao-ensino', component: InstituicaoEnsinoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
