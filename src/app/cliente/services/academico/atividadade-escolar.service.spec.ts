import { TestBed } from '@angular/core/testing';

import { AtividadadeEscolarService } from './atividadade-escolar.service';

describe('AtividadadeEscolarService', () => {
  let service: AtividadadeEscolarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AtividadadeEscolarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
