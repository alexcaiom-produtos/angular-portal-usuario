import { TestBed } from '@angular/core/testing';

import { AreaEstudoService } from './area-estudo.service';

describe('AreaEstudoService', () => {
  let service: AreaEstudoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AreaEstudoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
