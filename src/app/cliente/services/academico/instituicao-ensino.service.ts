import { Injectable } from '@angular/core';
import { Observable, throwError } from "rxjs";
import { retry, catchError } from "rxjs/operators";
import { InstituicaoEnsino } from "src/app/cliente/models/academico/instituicao-ensino";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Constantes } from '../../models/constantes';

@Injectable({
  providedIn: 'root'
})
export class InstituicaoEnsinoService {

  url = Constantes.BASE_URL+'/academico/instituicao-ensino';

  constructor(
    private rt: HttpClient
  ) { }

  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get() : Observable<InstituicaoEnsino[]> {
    return this.rt.get<InstituicaoEnsino[]>(this.url)
          .pipe(
            retry(2),
            catchError(this.handleError)
          );
  }

  add(instituicao: InstituicaoEnsino) : Observable<InstituicaoEnsino> {
    return this.rt.post<InstituicaoEnsino>(this.url, instituicao)
          .pipe(
            retry(2),
            catchError(this.handleError)
          );
  }

  edit(instituicao: InstituicaoEnsino) : Observable<InstituicaoEnsino> {
    return this.rt.put<InstituicaoEnsino>(this.url, instituicao)
          .pipe(
            retry(2),
            catchError(this.handleError)
          );
  }

  delete(id) : Observable<InstituicaoEnsino> {
    return this.rt.delete<InstituicaoEnsino>(this.url + "/"+id)
          .pipe(
            retry(2),
            catchError(this.handleError)
          );
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
