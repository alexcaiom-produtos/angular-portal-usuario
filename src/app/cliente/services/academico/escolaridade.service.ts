import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Escolaridade } from '../../models/academico/escolaridade';
import { InstituicaoEnsino } from '../../models/academico/instituicao-ensino';
import { Constantes } from '../../models/constantes';

@Injectable({
  providedIn: 'root'
})
export class EscolaridadeService {

  url = Constantes.BASE_URL+'/academico/escolaridade';

  constructor(
    private rt: HttpClient
  ) { }

  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get() : Observable<Escolaridade[]> {
    return this.rt.get<Escolaridade[]>(this.url)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }
  
  add(escolaridade: Escolaridade) : Observable<Escolaridade> {
    return this.rt.post<Escolaridade>(this.url, escolaridade)
    .pipe(
      retry(2),
      catchError(this.handleError)
      );
  }
  
  update(escolaridade: Escolaridade) : Observable<Escolaridade> {
    return this.rt.put<Escolaridade>(this.url, escolaridade)
    .pipe(
      retry(2),
      catchError(this.handleError)
      );
  }
  
  delete(id) : Observable<Escolaridade> {
    return this.rt.delete<Escolaridade>(this.url+'/id/'+id)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }
  
    niveis() : Observable<string[]> {
      return this.rt.get<string[]>(this.url+"/nivel")
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
    }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
