import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { AtividadeEscolar } from '../../models/academico/atividade-escolar';
import { Constantes } from '../../models/constantes';

@Injectable({
  providedIn: 'root'
})
export class AtividadadeEscolarService {

  url = Constantes.BASE_URL+'/academico/atividade-escolar';

  constructor(
    private rt: HttpClient
  ) { }

  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get() : Observable<AtividadeEscolar[]> {
    return this.rt.get<AtividadeEscolar[]>(this.url)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  add(atividade: AtividadeEscolar) : Observable<AtividadeEscolar> {
    return this.rt.post<AtividadeEscolar>(this.url, atividade)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }
  

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
