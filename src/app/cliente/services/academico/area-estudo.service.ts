import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { AreaDeEstudo } from '../../models/academico/area-de-estudo';
import { Constantes } from '../../models/constantes';

@Injectable({
  providedIn: 'root'
})
export class AreaEstudoService {

  url = Constantes.BASE_URL+'/academico/area-estudo';

  constructor(
    private rt: HttpClient
  ) { }

  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get() : Observable<AreaDeEstudo[]> {
    return this.rt.get<AreaDeEstudo[]>(this.url)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  
  add(area: AreaDeEstudo) : Observable<AreaDeEstudo> {
    debugger;
    return this.rt.post<AreaDeEstudo>(this.url, area)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  update(area: AreaDeEstudo) : Observable<AreaDeEstudo> {
    return this.rt.put<AreaDeEstudo>(this.url, area)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  
  delete(id: string) : Observable<AreaDeEstudo> {
    debugger;
    return this.rt.delete<AreaDeEstudo>(this.url+'/'+id)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
