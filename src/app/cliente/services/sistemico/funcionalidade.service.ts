import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Funcionalidade } from "src/app/cliente/models/sistemico/funcionalidade";
import { Constantes } from '../../models/constantes';

@Injectable({
  providedIn: 'root'
})
export class FuncionalidadeService {

  url = Constantes.BASE_URL+'/sistemico/funcionalidade';

  constructor(
    private rt: HttpClient
  ) { }

  
  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get() : Observable<Funcionalidade[]> {
    return this.rt.get<Funcionalidade[]>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  add(funcionalidade: Funcionalidade) : Observable<Funcionalidade> {
    return this.rt.post<Funcionalidade>(this.url, funcionalidade)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  update(funcionalidade: Funcionalidade) : Observable<Funcionalidade> {
    return this.rt.put<Funcionalidade>(this.url, funcionalidade)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  delete(id)  {
    debugger;
    return this.rt.delete(this.url+"/id/"+id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  handleError(error : HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
