import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Modulo } from "src/app/cliente/models/sistemico/modulo";
import { Constantes } from '../../models/constantes';

@Injectable({
  providedIn: 'root'
})
export class ModuloService {

  url = Constantes.BASE_URL+'/sistemico/modulo';

  constructor(
    private rt: HttpClient
  ) { }

  
  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get() : Observable<Modulo[]> {
    return this.rt.get<Modulo[]>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  add(modulo: Modulo) : Observable<Modulo> {
    return this.rt.post<Modulo>(this.url, modulo)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  update(modulo: Modulo) : Observable<Modulo> {
    return this.rt.put<Modulo>(this.url, modulo)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  delete(id)  {
    debugger;
    return this.rt.delete(this.url+"/id/"+id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  handleError(error : HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
