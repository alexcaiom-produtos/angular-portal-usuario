import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Sistema } from "src/app/cliente/models/sistemico/sistema";
// import { StarTemplateContext } from '@ng-bootstrap/ng-bootstrap/rating/rating';
import { Constantes } from "../../models/constantes";


@Injectable({
  providedIn: 'root'
})
export class SistemaService {

  url = Constantes.BASE_URL+'/sistemico/sistema';

  constructor(
    private rt: HttpClient
  ) { }

  
  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get() : Observable<Sistema[]> {
    return this.rt.get<Sistema[]>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  add(sistema: Sistema) : Observable<Sistema> {
    return this.rt.post<Sistema>(this.url, sistema)
      .pipe(
        // retry(2),
        catchError(this.handleError)
      )
  }

  update(sistema: Sistema) : Observable<Sistema> {
    return this.rt.put<Sistema>(this.url, sistema)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  delete(id)  {
    debugger;
    return this.rt.delete(this.url+"/id/"+id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  handleError(error : HttpErrorResponse) {
    debugger;
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
