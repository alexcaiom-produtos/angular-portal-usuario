import { Endereco } from "src/app/localizacao/endereco";
export interface Usuario {

    nome: string;
    email: string;
    enderecos: Endereco[];
    nascimento: Date;

}
