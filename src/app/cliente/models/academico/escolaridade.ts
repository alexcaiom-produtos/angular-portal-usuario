import { AreaDeEstudo } from "./area-de-estudo";
import { AtividadeEscolar } from "./atividade-escolar";
import { InstituicaoEnsino } from './instituicao-ensino';
export interface Escolaridade {
    id: number;
    nomeDoCurso: string;
    inicio: Date;
    fim: Date;
    area: AreaDeEstudo;
    atividades: AtividadeEscolar[];
    escolaridade: string;
    instituicao: InstituicaoEnsino;
}
