import { Endereco } from '../../../localizacao/endereco';
export interface InstituicaoEnsino {

    id: number;
    nome: string;
    inicio: Date;
    fim: Date;
    escolaridade: string;
    selecionado: string;
    endereco: Endereco;
    logradouro: string;

}
