import { Modulo } from "./modulo";

export interface Funcionalidade {
    id: number;
    nome: string;
    modulo: Modulo;
}
