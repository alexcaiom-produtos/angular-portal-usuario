import { Funcionalidade } from './funcionalidade';
import { Sistema } from './sistema';

export interface Modulo {
    id: number;
    nome: string;
    sistema: Sistema;
    funcionalidades: Funcionalidade[];
}
