import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Usuario } from '../models/usuario';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Constantes } from '../models/constantes';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url = Constantes.BASE_URL+'/usuario';

  constructor(
    private rest: HttpClient
  ) { }

  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  
  get() : Observable<Usuario[]> {
    return this.rest.get<Usuario[]>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  handleError(error : HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
