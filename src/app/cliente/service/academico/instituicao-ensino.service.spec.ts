import { TestBed } from '@angular/core/testing';

import { InstituicaoEnsinoService } from './instituicao-ensino.service';

describe('InstituicaoEnsinoService', () => {
  let service: InstituicaoEnsinoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InstituicaoEnsinoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
