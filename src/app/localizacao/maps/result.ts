import { AddressComponents } from "./address-components";
import { Geometry } from "./geometry";
export interface Result {

    address_components: AddressComponents[];
    formatted_address: string;
    geometry: Geometry;

}
