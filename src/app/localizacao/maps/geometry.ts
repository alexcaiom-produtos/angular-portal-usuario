import { Location } from "./location";
export interface Geometry {
    location: Location;
}
