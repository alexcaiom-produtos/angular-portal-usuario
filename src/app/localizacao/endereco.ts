import { CoordenadasGeograficas } from "./coordenadas-geograficas";
export interface Endereco {
    id: string;
    logradouro: string;
    coordenadasGeograficas: CoordenadasGeograficas;
    numero: number;
    complemento: string;
    tipo: string;
    cep: string;
    bairro: string;
    cidade: string;
    estado: string;
    pais: string;
}
