export interface CoordenadasGeograficas {
    latitude: number;
    longitude: number;
}
