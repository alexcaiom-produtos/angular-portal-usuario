import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Response } from "src/app/localizacao/maps/response";

@Injectable({
  providedIn: 'root'
})
export class LogradouroService {

  urlBase = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBhuoOhesFTHOfnGLHuf0ajA7iF4J60tEU';
  url: string;
  constructor(
    private rest: HttpClient
  ) { }

  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application-json'})
  }

  get(latitude: string, longitude: string) : Observable<Response> {
    this.url = this.urlBase + "&latlng=" + latitude + "," + longitude;
    console.log(this.url);
    return this.rest.get<Response>(this.url)
            .pipe(
              retry(2),
              catchError(this.handleError)
            )
  }

  getLogradouro(logradouro: string) : Observable<Response> {
    this.url = this.urlBase + "&address=" + logradouro;
    console.log(this.url);
    return this.rest.get<Response>(this.url)
            .pipe(
              retry(2),
              catchError(this.handleError)
            )
  }

  
  handleError(error : HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
