import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FuncionalidadeService } from "src/app/cliente/services/sistemico/funcionalidade.service";
import { Funcionalidade } from "src/app/cliente/models/sistemico/funcionalidade";
import { ClienteFormComponent } from '../../cliente-form/cliente-form.component';
import { FuncionalidadeFormComponent } from '../funcionalidade-form/funcionalidade-form.component';


@Component({
  selector: 'app-funcionalidade',
  templateUrl: './funcionalidade.component.html',
  styleUrls: ['./funcionalidade.component.css']
})
export class FuncionalidadeComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private service: FuncionalidadeService
  ) { }

  
  funcionalidades : Funcionalidade[] = [];
  insercao: boolean = false;
  funcionalidade : Funcionalidade;

  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((funcionalidades : Funcionalidade[]) => {
      this.funcionalidades = funcionalidades;
    });
  }

  add() {
    const modal = this.modalService.open(FuncionalidadeFormComponent);
  }
  
  editar(funcionalidade: Funcionalidade) {
    const modal = this.modalService.open(FuncionalidadeFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
    modal.componentInstance.insercao = false;
    modal.componentInstance.funcionalidade = funcionalidade;
  }

  handleModalForm(response) {
    debugger;
    this.show();
  }

  deletar(id: string, index: number) {
    if (confirm("Tem certeza de que deseja excluir?")) {
      this.service.delete(id).subscribe(response => {
        this.funcionalidades.splice(index, 1); 
      });
    }
  }
}
