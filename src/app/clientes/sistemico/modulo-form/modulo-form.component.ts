import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Modulo } from 'src/app/cliente/models/sistemico/modulo';
import { Sistema } from 'src/app/cliente/models/sistemico/sistema';
import { ModuloService } from 'src/app/cliente/services/sistemico/modulo.service';
import { SistemaService } from 'src/app/cliente/services/sistemico/sistema.service';

@Component({
  selector: 'app-modulo-form',
  templateUrl: './modulo-form.component.html',
  styleUrls: ['./modulo-form.component.css']
})
export class ModuloFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  modulo : Modulo;
  titulo : string;
  sistemas : Sistema[];
  idSistema: string;
  sistema : Sistema;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private serviceSistema: SistemaService,
    private service: ModuloService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id : [''],
      nome : ['', Validators.required],
      idSistema : ['']
    });
    if (!this.insercao) {
      this.load(this.modulo);
    }
    if (this.modulo && this.modulo.nome) {
      this.titulo = this.modulo.nome;
    } else {
      this.titulo = 'Cadastro de Modulo';
    }
    this.serviceSistema.get().subscribe((sistemas : Sistema[]) => {
      // debugger;
      this.sistemas = sistemas;
    });
  }

  load(modulo: Modulo) {
    this.form.patchValue(modulo);
  }

  save() {
    debugger;
    if (this.form.invalid) {
      return;
    }
    this.setSistema();
    this.modulo = this.form.value;
    this.modulo.sistema = this.sistema;
    if (this.insercao) {
      this.service.add(this.modulo).subscribe((modulo : Modulo) => {
        this.modulo = modulo;
        this.fechar(modulo);
      });
      
    } else {
      this.service.update(this.modulo).subscribe((modulo : Modulo) => {
        this.modulo = modulo;
        this.fechar(modulo);
      });
    }
  }

  // setSistema(sistema: Sistema) {
  setSistema() {
    debugger;
    for(let i = 0; i < this.sistemas.length; i++) {
      if (this.sistemas[i].id.toString() == this.form.controls.idSistema.value) {
        this.sistema = this.sistemas[i];
      }
    }
  }
  
  
  fechar(modulo: Modulo) {
    debugger;
    this.activeModal.dismiss({modulo, CreateMode: false});
  }

}
