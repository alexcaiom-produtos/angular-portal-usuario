import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModuloService } from 'src/app/cliente/services/sistemico/modulo.service';
import { Modulo } from 'src/app/cliente/models/sistemico/modulo';
import { ClienteFormComponent } from '../../cliente-form/cliente-form.component';
import { ModuloFormComponent } from '../modulo-form/modulo-form.component';

@Component({
  selector: 'app-modulo',
  templateUrl: './modulo.component.html',
  styleUrls: ['./modulo.component.css']
})
export class ModuloComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private service: ModuloService
  ) { }

  
  modulos : Modulo[] = [];
  insercao: boolean = false;
  modulo : Modulo;

  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((modulos : Modulo[]) => {
      this.modulos = modulos;
    });
  }

  add() {
    const modal = this.modalService.open(ModuloFormComponent);
  }
  
  editar(modulo: Modulo) {
    const modal = this.modalService.open(ModuloFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this));
    modal.componentInstance.insercao = false;
    modal.componentInstance.modulo = modulo;
  }

  handleModalForm(response) {
    debugger;
    this.show();
  }

  deletar(id: string, index: number) {
    if (confirm("Tem certeza de que deseja excluir?")) {
      this.service.delete(id)
      .subscribe(response => {
        this.modulos.splice(index, 1); 
      });
    }
  }
}
