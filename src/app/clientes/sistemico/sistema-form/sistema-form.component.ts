import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Sistema } from "src/app/cliente/models/sistemico/sistema";
import { SistemaService } from 'src/app/cliente/services/sistemico/sistema.service';

@Component({
  selector: 'app-sistema-form',
  templateUrl: './sistema-form.component.html',
  styleUrls: ['./sistema-form.component.css']
})
export class SistemaFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  sistema : Sistema;
  titulo : string;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private service: SistemaService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nome : ['', Validators.required]
    });
    if (!this.insercao) {
      this.load(this.sistema);
    }
    if (this.sistema && this.sistema.nome) {
      this.titulo = this.sistema.nome;
    } else {
      this.titulo = 'Cadastro de Sistema';
    }
  }

  load(sistema: Sistema) {
    this.form.patchValue(sistema);
  }

  save() {
    debugger;
    if (this.form.invalid) {
      return;
    }
    
    let sistema : Sistema = this.form.value;
    if (this.insercao) {
      this.service.add(sistema).subscribe((sistema : Sistema) => {
        this.fechar(sistema);
      });
    } else {
      this.service.update(sistema).subscribe((sistema : Sistema) => {
        this.fechar(sistema);
      });
    }
  }
  
  fechar(sistema: Sistema) {
    this.activeModal.dismiss({sistema, CreateMode: true})
  }

}
