import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Sistema } from "src/app/cliente/models/sistemico/sistema";
import { SistemaService } from "src/app/cliente/services/sistemico/sistema.service";
import { SistemaFormComponent } from '../sistema-form/sistema-form.component';

@Component({
  selector: 'app-sistema',
  templateUrl: './sistema.component.html',
  styleUrls: ['./sistema.component.css']
})
export class SistemaComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private service: SistemaService
  ) { }

  sistemas : Sistema[] = [];
  insercao: boolean = false;
  sistema : Sistema;

  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((sistemas : Sistema[]) => {
      this.sistemas = sistemas;
    });
  }

  add() {
    const modal = this.modalService.open(SistemaFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
  }

  
  editar(sistema: Sistema) {
    const modal = this.modalService.open(SistemaFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
    modal.componentInstance.insercao = false;
    modal.componentInstance.sistema = sistema;
  }

  handleModalForm(response) {
    
  }

  deletar(id: string, index: number) {
    if (confirm("Tem certeza de que deseja excluir?")) {
      this.service.delete(id)
      .subscribe(response => {
        this.sistemas.splice(index, 1); 
      });
    }
  }
}
