import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionalidadeFormComponent } from './funcionalidade-form.component';

describe('FuncionalidadeFormComponent', () => {
  let component: FuncionalidadeFormComponent;
  let fixture: ComponentFixture<FuncionalidadeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionalidadeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionalidadeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
