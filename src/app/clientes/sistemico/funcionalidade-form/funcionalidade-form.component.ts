import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Funcionalidade } from 'src/app/cliente/models/sistemico/funcionalidade';
import { Modulo } from 'src/app/cliente/models/sistemico/modulo';
import { FuncionalidadeService } from 'src/app/cliente/services/sistemico/funcionalidade.service';
import { ModuloService } from 'src/app/cliente/services/sistemico/modulo.service';

@Component({
  selector: 'app-funcionalidade-form',
  templateUrl: './funcionalidade-form.component.html',
  styleUrls: ['./funcionalidade-form.component.css']
})
export class FuncionalidadeFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  funcionalidade : Funcionalidade;
  modulos: Modulo[];
  modulo: Modulo;
  idModulo: string;
  titulo : string;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private service: FuncionalidadeService,
    private moduloService: ModuloService
  ) { }

  ngOnInit(): void {
    debugger;
    this.form = this.formBuilder.group({
      nome : ['', Validators.required],
      idModulo : ['', Validators.required]
    });
    if (!this.insercao) {
      this.load(this.funcionalidade);
    }
    if (this.funcionalidade && this.funcionalidade.nome) {
      this.titulo = this.funcionalidade.nome;
    } else {
      this.titulo = 'Cadastro de Funcionalidade';
    }
    
    this.moduloService.get().subscribe((modulos : Modulo[]) => {
      // debugger;
      this.modulos = modulos;
    });
  }

  load(funcionalidade: Funcionalidade) {
    this.form.patchValue(funcionalidade);
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    this.setModulo();
    this.funcionalidade = this.form.value;
    this.funcionalidade.modulo = this.modulo;
    if (this.insercao) {
      this.service.add(this.funcionalidade).subscribe((funcionalidade : Funcionalidade) => {
        this.funcionalidade = funcionalidade;
        this.fechar(funcionalidade);
      });
      
    } else {
      this.service.update(this.funcionalidade).subscribe((funcionalidade : Funcionalidade) => {
        this.funcionalidade = funcionalidade;
        this.fechar(funcionalidade);
      });
    }
  }

  // setSistema(sistema: Sistema) {
  setModulo() {
    debugger;
    for(let i = 0; i < this.modulos.length; i++) {
      if (this.modulos[i].id.toString() == this.form.controls.idModulo.value) {
        this.modulo = this.modulos[i];
      }
    }
  }

  
  fechar(funcionalidade: Funcionalidade) {
    this.activeModal.dismiss({funcionalidade, CreateMode: false})
  }

}
