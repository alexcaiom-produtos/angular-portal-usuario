import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Usuario } from 'src/app/cliente/models/usuario';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.css']
})
export class ClienteFormComponent implements OnInit {

  titulo: String = "Cadastro";
  insercao: boolean = true;
  form : FormGroup;
  usuario: Usuario;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nome : ['', Validators.required],
      endereco : ['', null]
    });
    if (!this.insercao) {
      this.load(this.usuario);
    }
    if (this.usuario && this.usuario.nome) {
      this.titulo = this.usuario.nome; 
    } else {
      this.titulo = 'Cadastro de Clientes';
    }
  }


  load(usuario : Usuario) {
    this.form.patchValue(usuario);
  }

  save() {
    if (this.form.invalid) {
      return;
    }
  }

  close() {
    this.activeModal.close('Close click');
  }
}
