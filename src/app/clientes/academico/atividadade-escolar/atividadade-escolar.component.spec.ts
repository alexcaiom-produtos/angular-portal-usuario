import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtividadadeEscolarComponent } from './atividadade-escolar.component';

describe('AtividadadeEscolarComponent', () => {
  let component: AtividadadeEscolarComponent;
  let fixture: ComponentFixture<AtividadadeEscolarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtividadadeEscolarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtividadadeEscolarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
