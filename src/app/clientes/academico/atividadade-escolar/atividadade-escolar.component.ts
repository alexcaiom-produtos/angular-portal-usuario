import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AtividadadeEscolarService } from 'src/app/cliente/services/academico/atividadade-escolar.service'
import { AtividadeEscolar } from 'src/app/cliente/models/academico/atividade-escolar';
import { AtividadadeEscolarFormComponent } from '../atividadade-escolar-form/atividadade-escolar-form.component';

@Component({
  selector: 'app-atividadade-escolar',
  templateUrl: './atividadade-escolar.component.html',
  styleUrls: ['./atividadade-escolar.component.css']
})
export class AtividadadeEscolarComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private service: AtividadadeEscolarService
  ) { }

  atividades: AtividadeEscolar[] = [];
  insercao: boolean = false;
  atividade: AtividadeEscolar;

  
  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((atividades : AtividadeEscolar[]) => {
      // debugger;
      // for (let index = 0; index < escolaridades.length; index++) {
      //   let inicio: Date = new Date(escolaridades[index].inicio);
      //   escolaridades[index].inicio = inicio;
      // }
      this.atividades = atividades;
    });
  }

  add() {
    const modal = this.modalService.open(AtividadadeEscolarFormComponent);
  }

  
  editar(atividade: AtividadeEscolar) {
    const modal = this.modalService.open(AtividadadeEscolarFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
    modal.componentInstance.insercao = false;
    modal.componentInstance.atividade = atividade;
  }

  handleModalForm(response) {
    
  }

  deletar(id: string, index: number) {
    if (confirm("Tem certeza de que deseja excluir?")) {
      // this.service.delete(id)
      // .subscribe(response => {
      //   this.escolaridades.splice(index, 1); 
      // });
    }
  }

}
