import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AreaDeEstudo } from 'src/app/cliente/models/academico/area-de-estudo';
import { AreaEstudoService } from 'src/app/cliente/services/academico/area-estudo.service';

@Component({
  selector: 'app-area-estudo-form',
  templateUrl: './area-estudo-form.component.html',
  styleUrls: ['./area-estudo-form.component.css']
})
export class AreaEstudoFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  area : AreaDeEstudo;
  titulo : string;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private service: AreaEstudoService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nome : ['', Validators.required]
    });
    if (!this.insercao) {
      this.load(this.area);
    }
    if (this.area && this.area.nome) {
      this.titulo = this.area.nome;
    } else {
      this.titulo = 'Cadastro de Area de Estudo';
    }
  }

  load(area: AreaDeEstudo) {
    this.form.patchValue(area);
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    let area : AreaDeEstudo = this.form.value;
    debugger;
    if (this.insercao) {
      this.service.add(area).subscribe((area : AreaDeEstudo) => {
        this.area = area;
        this.fechar(area);
      });
      
    } else {
      this.service.update(area).subscribe((area : AreaDeEstudo) => {
        this.area = area;
        this.fechar(area);
      });
    }
    this.activeModal.dismiss({area, CreateMode: true})
  }
  
  fechar(area: AreaDeEstudo) {
    debugger;
    this.activeModal.dismiss({area, CreateMode: false});
  }
}
