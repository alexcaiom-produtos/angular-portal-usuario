import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaEstudoFormComponent } from './area-estudo-form.component';

describe('AreaEstudoFormComponent', () => {
  let component: AreaEstudoFormComponent;
  let fixture: ComponentFixture<AreaEstudoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaEstudoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaEstudoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
