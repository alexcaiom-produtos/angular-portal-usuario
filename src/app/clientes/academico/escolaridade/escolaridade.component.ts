import {
  ChangeDetectionStrategy,
  ChangeDetectorRef, 
  Component, 
  OnInit ,
  OnDestroy
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

//Components
import { MatCalendar } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MatDateFormats } from '@angular/material/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
//Components

import { Escolaridade } from "src/app/cliente/models/academico/escolaridade";
import { EscolaridadeService } from 'src/app/cliente/services/academico/escolaridade.service';
import { EscolaridadeFormComponent } from '../escolaridade-form/escolaridade-form.component';

@Component({
  selector: 'app-escolaridade',
  templateUrl: './escolaridade.component.html',
  styleUrls: ['./escolaridade.component.css']
})
export class EscolaridadeComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private service: EscolaridadeService
  ) { }

  escolaridades: Escolaridade[] = [];
  insercao: boolean = false;
  escolaridade: Escolaridade;

  
  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((escolaridades : Escolaridade[]) => {
      debugger;
      // for (let index = 0; index < escolaridades.length; index++) {
      //   let inicio: Date = new Date(escolaridades[index].inicio);
      //   escolaridades[index].inicio = inicio;
      // }
      this.escolaridades = escolaridades;
    });
  }

  add() {
    const modal = this.modalService.open(EscolaridadeFormComponent);
  }

  
  editar(escolaridade: Escolaridade) {
    const modal = this.modalService.open(EscolaridadeFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
    modal.componentInstance.insercao = false;
    modal.componentInstance.escolaridade = escolaridade;
  }

  handleModalForm(response) {
    
  }

  deletar(id: string, index: number) {
    if (confirm("Tem certeza de que deseja excluir?")) {
      // this.service.delete(id)
      // .subscribe(response => {
      //   this.escolaridades.splice(index, 1); 
      // });
    }
  }

}
