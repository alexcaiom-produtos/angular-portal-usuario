import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EscolaridadeFormComponent } from './escolaridade-form.component';

describe('EscolaridadeFormComponent', () => {
  let component: EscolaridadeFormComponent;
  let fixture: ComponentFixture<EscolaridadeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EscolaridadeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscolaridadeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
