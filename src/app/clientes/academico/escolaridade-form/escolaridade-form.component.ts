import {
  ChangeDetectionStrategy,
  ChangeDetectorRef, 
  Component, 
  OnInit ,
  OnDestroy
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Escolaridade } from 'src/app/cliente/models/academico/escolaridade';
import { InstituicaoEnsino } from 'src/app/cliente/models/academico/instituicao-ensino';
import { EscolaridadeService } from 'src/app/cliente/services/academico/escolaridade.service';
import { InstituicaoEnsinoService } from 'src/app/cliente/services/academico/instituicao-ensino.service';

@Component({
  selector: 'app-escolaridade-form',
  templateUrl: './escolaridade-form.component.html',
  styleUrls: ['./escolaridade-form.component.css']
})
export class EscolaridadeFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  escolaridade : Escolaridade;
  instituicoes: InstituicaoEnsino[];
  idInstituicao: string;
  instituicao: InstituicaoEnsino;
  niveis : string[];
  titulo : string;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private service: EscolaridadeService,
    private instituicaoService: InstituicaoEnsinoService
  ) { }

  ngOnInit(): void {
    debugger;
    this.form = this.formBuilder.group({
      nome : ['', Validators.required],
      idInstituicao : [''],
      inicio : [''],
      fim : [''],
      nivel : ['', Validators.required]

    });
    if (!this.insercao) {
      this.load(this.escolaridade);
    }
    if (this.escolaridade && this.escolaridade.nomeDoCurso) {
      this.titulo = this.escolaridade.nomeDoCurso;
    } else {
      this.titulo = 'Cadastro de Escolaridade';
    }
    this.instituicaoService.get().subscribe((instituicoes : InstituicaoEnsino[]) => {
      debugger;
      this.instituicoes = instituicoes;
      for (let i = 0; i < this.instituicoes.length; i++) {
        if (this.instituicoes[i].id == this.escolaridade.instituicao.id) {
          this.instituicoes[i].selecionado = "selected";
        }
      }
    });

    this.service.niveis().subscribe((niveis : string[]) => {
      // debugger;
      this.niveis = niveis;
    });


  }

  load(escolaridade: Escolaridade) {
    this.form.patchValue(escolaridade);
    if(escolaridade.instituicao){
      this.instituicao = escolaridade.instituicao;
    }
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    this.setInstituicao();
    this.escolaridade = this.form.value;
    this.escolaridade.instituicao = this.instituicao;
    if (this.insercao) {
      this.service.add(this.escolaridade).subscribe((escolaridade : Escolaridade) => {
        this.escolaridade = escolaridade;
        this.fechar(escolaridade);
      });
      
    } else {
      this.service.update(this.escolaridade).subscribe((escolaridade : Escolaridade) => {
        this.escolaridade = escolaridade;
        this.fechar(escolaridade);
      });
    }
  }

  // setSistema(sistema: Sistema) {
  setInstituicao() {
    debugger;
    for(let i = 0; i < this.instituicoes.length; i++) {
      if (this.instituicoes[i].id.toString() == this.form.controls.idInstituicao.value) {
        this.instituicao = this.instituicoes[i];
      }
    }
  }
  
  
  fechar(escolaridade: Escolaridade) {
    this.activeModal.dismiss({escolaridade, CreateMode: true})
  }
}
