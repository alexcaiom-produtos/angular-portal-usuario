import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtividadadeEscolarFormComponent } from './atividadade-escolar-form.component';

describe('AtividadadeEscolarFormComponent', () => {
  let component: AtividadadeEscolarFormComponent;
  let fixture: ComponentFixture<AtividadadeEscolarFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtividadadeEscolarFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtividadadeEscolarFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
