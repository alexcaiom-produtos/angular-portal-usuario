import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AreaDeEstudo } from 'src/app/cliente/models/academico/area-de-estudo';
import { AtividadeEscolar } from 'src/app/cliente/models/academico/atividade-escolar';
import { Escolaridade } from 'src/app/cliente/models/academico/escolaridade';
import { AtividadadeEscolarService } from 'src/app/cliente/services/academico/atividadade-escolar.service';
import { EscolaridadeService } from 'src/app/cliente/services/academico/escolaridade.service';

@Component({
  selector: 'app-atividadade-escolar-form',
  templateUrl: './atividadade-escolar-form.component.html',
  styleUrls: ['./atividadade-escolar-form.component.css']
})
export class AtividadadeEscolarFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  atividade : AtividadeEscolar;
  titulo : string;
  escolaridades: Escolaridade[];
  idEscolaridade: string;
  escolaridade: Escolaridade;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private service: AtividadadeEscolarService,
    private serviceEscolaridade: EscolaridadeService
  ) { }

  ngOnInit(): void {
    debugger;
    this.form = this.formBuilder.group({
      nome : ['', Validators.required],
      descricao : ['', Validators.required],
      idEscolaridade : ['']
    });
    if (!this.insercao) {
      this.load(this.atividade);
    }
    if (this.atividade && this.atividade.nome) {
      this.titulo = this.atividade.nome;
    } else {
      this.titulo = 'Cadastro de Atividade Escolar';
    }

    this.serviceEscolaridade.get().subscribe((escolaridades : Escolaridade[]) => {
      debugger;
      this.escolaridades = escolaridades;
    });
  }

  load(atividade: AtividadeEscolar) {
    this.form.patchValue(atividade);
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    
    this.service.add(this.atividade);
  }

  
  setEscolaridade() {
    debugger;
    for(let i = 0; i < this.escolaridades.length; i++) {
      if (this.escolaridades[i].id.toString() == this.form.controls.idInstituicao.value) {
        this.escolaridade = this.escolaridades[i];
      }
    }
  }
}
