import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InstituicaoEnsino } from 'src/app/cliente/models/academico/instituicao-ensino';
import { LogradouroService } from 'src/app/localizacao/services/logradouro.service';
import { InstituicaoEnsinoService } from 'src/app/cliente/services/academico/instituicao-ensino.service';
import { ClienteFormComponent } from '../../cliente-form/cliente-form.component';
import { Response } from "src/app/localizacao/maps/response";
import { InstituicaoEnsinoFormComponent } from '../instituicao-ensino-form/instituicao-ensino-form.component';

@Component({
  selector: 'app-instituicao-ensino',
  templateUrl: './instituicao-ensino.component.html',
  styleUrls: ['./instituicao-ensino.component.css']
})
export class InstituicaoEnsinoComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private service: InstituicaoEnsinoService,
    private logradouroService: LogradouroService
  ) { }

  instituicoes: InstituicaoEnsino[] = [];
  insercao: boolean = false;
  instituicao: InstituicaoEnsino;

  
  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((instituicoes : InstituicaoEnsino[]) => {
      for (let index = 0; index < instituicoes.length; index++) {
        if (null != instituicoes[index].endereco) {
          const element = instituicoes[index].endereco;
           this.logradouroService.get(element.coordenadasGeograficas.latitude.toString(), 
                                    element.coordenadasGeograficas.longitude.toString())
                      .subscribe((response : Response) => {
                        let logradouro: string;
                        let numero: number;
                        let complemento: string;
                        let tipo: string;
                        let cep: string;
                        let bairro: string;
                        let cidade: string;
                        let estado: string;
                        let pais: string;
                        for (let index3 = 0; index3 < response.results.length; index3++) {
                          const resultado = response.results[index3];
                          for (let index4 = 0; index4 < resultado.address_components.length; index4++) {
                            const componente = resultado.address_components[index4];
                            switch(componente.types[0]){
                              case "street_number" : {
                                numero = Number(componente.long_name);
                                break;
                              }
                              case "route" : {
                                logradouro = componente.long_name;
                                break;
                              }
                              case "political" : {
                                bairro = componente.long_name;
                                break;
                              }
                              case "administrative_area_level_2" : {
                                cidade = componente.long_name;
                                break;
                              }
                              case "administrative_area_level_1" : {
                                estado = componente.long_name;
                                break;
                              }
                              case "country" : {
                                pais = componente.long_name;
                                break;
                              }
                              case "postal_code" : {
                                cep = componente.long_name;
                                break;
                              }
                            }
                          }
                        }
                        instituicoes[index].endereco.logradouro = logradouro;
                        //instituicoes[index].endereco.numero = numero;
                        instituicoes[index].endereco.complemento = complemento;
                        instituicoes[index].endereco.tipo = tipo;
                        instituicoes[index].endereco.cep = cep;
                        instituicoes[index].endereco.bairro = bairro;
                        instituicoes[index].endereco.cidade = cidade;
                        instituicoes[index].endereco.estado = estado;
                        instituicoes[index].endereco.pais = pais;
                      });
        }        
      }
      this.instituicoes = instituicoes;
    });
  }

  add() {
    const modal = this.modalService.open(InstituicaoEnsinoFormComponent);
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
  }

  
  editar(instituicao: InstituicaoEnsino) {
    const modal = this.modalService.open(InstituicaoEnsinoFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
    modal.componentInstance.insercao = false;
    modal.componentInstance.instituicao = instituicao;
  }

  handleModalForm(response) {
    
  }

  deletar(id: string, index: number) {
    if (confirm("Tem certeza de que deseja excluir?")) {
      // this.service.delete(id)
      // .subscribe(response => {
      //   this.sistemas.splice(index, 1); 
      // });
    }
  }

}
