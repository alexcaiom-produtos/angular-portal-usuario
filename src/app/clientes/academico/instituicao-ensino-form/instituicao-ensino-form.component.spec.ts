import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstituicaoEnsinoFormComponent } from './instituicao-ensino-form.component';

describe('InstituicaoEnsinoFormComponent', () => {
  let component: InstituicaoEnsinoFormComponent;
  let fixture: ComponentFixture<InstituicaoEnsinoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstituicaoEnsinoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstituicaoEnsinoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
