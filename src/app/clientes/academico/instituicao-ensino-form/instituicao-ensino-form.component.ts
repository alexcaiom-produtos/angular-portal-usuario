import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InstituicaoEnsino } from 'src/app/cliente/models/academico/instituicao-ensino';
import { Endereco } from 'src/app/localizacao/endereco';
import { LogradouroService } from 'src/app/localizacao/services/logradouro.service';
import { Response } from 'src/app/localizacao/maps/response';
import { CoordenadasGeograficas } from 'src/app/localizacao/coordenadas-geograficas';
import { InstituicaoEnsinoService } from 'src/app/cliente/services/academico/instituicao-ensino.service';

@Component({
  selector: 'app-instituicao-ensino-form',
  templateUrl: './instituicao-ensino-form.component.html',
  styleUrls: ['./instituicao-ensino-form.component.css']
})
export class InstituicaoEnsinoFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  instituicao : InstituicaoEnsino;
  titulo : string;
  endereco: Endereco;
  enderecos: Endereco[] = new Array();
  logradouro: string;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private service: InstituicaoEnsinoService,
    private logradouroService: LogradouroService
  ) { }

  ngOnInit(): void {
    debugger;
    this.form = this.formBuilder.group({
      nome : ['', Validators.required],
      logradouro : [''],
      cep : ['']
    });
    if (!this.insercao) {
      this.load(this.instituicao);
    }
    if (this.instituicao && this.instituicao.nome) {
      this.titulo = this.instituicao.nome;
    } else {
      this.titulo = 'Cadastro de Funcionalidade';
    }
  }

  load(instituicao: InstituicaoEnsino) {
    if (instituicao.endereco) {
      this.instituicao.logradouro = instituicao.endereco.logradouro
      + ", "
      + instituicao.endereco.numero;
    }
    this.form.patchValue(instituicao);

  }

  save() {
    if (this.form.invalid) {
      return;
    }
    this.instituicao = this.form.value;
    this.service.add(this.instituicao);
  }

  setLogradouro() {
    debugger;
    for(let i = 0; i < this.enderecos.length; i++) {
      if (this.enderecos[i].cep.toString() == this.form.controls.cep.value) {
        this.endereco = this.enderecos[i];
      }
    }
  }

  pesquisarCep() {
    debugger;
    this.logradouroService.getLogradouro(this.form.controls.logradouro.value).subscribe((response : Response) => {
      debugger;
      this.enderecos = new Array();
      let logradouro: string;
      let numero: number;
      let cep: string;
      let bairro: string;
      let cidade: string;
      debugger;
      for (let index3 = 0; index3 < response.results.length; index3++) {
        const resultado = response.results[index3];
        for (let index4 = 0; index4 < resultado.address_components.length; index4++) {
          const componente = resultado.address_components[index4];
          switch(componente.types[0]){
            case "street_number" : {
              numero = Number(componente.long_name);
              break;
            }
            case "route" : {
              logradouro = componente.long_name;
              break;
            }
            case "political" : {
              bairro = componente.long_name;
              break;
            }
            case "administrative_area_level_2" : {
              cidade = componente.long_name;
              break;
            }
            case "postal_code" : {
              cep = componente.long_name;
              break;
            }
          }
        }
        debugger;
        let coordenadasGeograficas: CoordenadasGeograficas = {
          latitude : resultado.geometry.location.lat,
          longitude : resultado.geometry.location.lng
        };
        let endereco: Endereco = {
          id: null,
          logradouro: resultado.formatted_address,
          bairro : bairro,
          cep : cep,
          cidade : cidade,
          complemento : "",
          coordenadasGeograficas : coordenadasGeograficas,
          estado : "",
          numero : numero,
          pais : "",
          tipo : ""
        };
        this.enderecos.push(endereco);
      }
      
    });
    // for(let i = 0; i < this.enderecos.length; i++) {
    //   if (this.enderecos[i].cep.toString() == this.form.controls.cep.value) {
    //     this.endereco = this.enderecos[i];
    //   }
    // }
  }

}
