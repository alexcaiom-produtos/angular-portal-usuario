import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AreaDeEstudo } from "src/app/cliente/models/academico/area-de-estudo";
import { AreaEstudoService } from 'src/app/cliente/services/academico/area-estudo.service';
import { AreaEstudoFormComponent } from '../area-estudo-form/area-estudo-form.component';

@Component({
  selector: 'app-area-estudo',
  templateUrl: './area-estudo.component.html',
  styleUrls: ['./area-estudo.component.css']
})
export class AreaEstudoComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private service: AreaEstudoService
  ) { }

  areas: AreaDeEstudo[] = [];
  insercao: boolean = false;
  area: AreaDeEstudo;

  
  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((areas : AreaDeEstudo[]) => {
      this.areas = areas;
    });
  }

  add() {
    const modal = this.modalService.open(AreaEstudoFormComponent); 
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
  }

  
  editar(area: AreaDeEstudo) {
    const modal = this.modalService.open(AreaEstudoFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
    modal.componentInstance.insercao = false;
    modal.componentInstance.area = area;
  }

  handleModalForm(response) {
    this.show();
  }

  deletar(id: string, index: number) {
    if (confirm("Tem certeza de que deseja excluir?")) {
      this.service.delete(id)
      .subscribe(response => {
        this.areas.splice(index, 1); 
      });
    }
  }
}
