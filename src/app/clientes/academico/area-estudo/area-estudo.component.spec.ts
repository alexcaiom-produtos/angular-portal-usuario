import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaEstudoComponent } from './area-estudo.component';

describe('AreaEstudoComponent', () => {
  let component: AreaEstudoComponent;
  let fixture: ComponentFixture<AreaEstudoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaEstudoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaEstudoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
