import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClienteFormComponent } from '../cliente-form/cliente-form.component';
import { Usuario } from 'src/app/cliente/models/usuario';
import { UsuarioService } from 'src/app/cliente/service/usuario.service';
import { LogradouroService } from "src/app/localizacao/services/logradouro.service";
import { Response } from "src/app/localizacao/maps/response";


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  
  constructor(
    private modalService : NgbModal,
    private service: UsuarioService,
    private logradouroService: LogradouroService
  ) { }

  usuarios: Usuario[] = [];
  insercao: boolean = false;
  usuario: Usuario;

  ngOnInit(): void {
    this.show();
  }


  show() {
    this.service.get().subscribe((usuarios : Usuario[]) => {
      for (let index = 0; index < usuarios.length; index++) {
        for (let subIndex = 0; null!=usuarios[index].enderecos && subIndex < usuarios[index].enderecos.length; subIndex++) {
          const element = usuarios[index].enderecos[subIndex];
           this.logradouroService.get(element.coordenadasGeograficas.latitude.toString(), element.coordenadasGeograficas.longitude.toString())
                      .subscribe((response : Response) => {
                        let logradouro: string;
                        let numero: number;
                        let complemento: string;
                        let tipo: string;
                        let cep: string;
                        let bairro: string;
                        let cidade: string;
                        let estado: string;
                        let pais: string;
                        for (let index3 = 0; index3 < response.results.length; index3++) {
                          const resultado = response.results[index3];
                          for (let index4 = 0; index4 < resultado.address_components.length; index4++) {
                            const componente = resultado.address_components[index4];
                            switch(componente.types[0]){
                              case "street_number" : {
                                numero = Number(componente.long_name);
                                break;
                              }
                              case "route" : {
                                logradouro = componente.long_name;
                                break;
                              }
                              case "political" : {
                                bairro = componente.long_name;
                                break;
                              }
                              case "administrative_area_level_2" : {
                                cidade = componente.long_name;
                                break;
                              }
                              case "administrative_area_level_1" : {
                                estado = componente.long_name;
                                break;
                              }
                              case "country" : {
                                pais = componente.long_name;
                                break;
                              }
                              case "postal_code" : {
                                cep = componente.long_name;
                                break;
                              }
                            }
                          }
                        }
                        usuarios[index].enderecos[subIndex].logradouro = logradouro;
                        //usuarios[index].enderecos[subIndex].numero = numero;
                        usuarios[index].enderecos[subIndex].complemento = complemento;
                        usuarios[index].enderecos[subIndex].tipo = tipo;
                        usuarios[index].enderecos[subIndex].cep = cep;
                        usuarios[index].enderecos[subIndex].bairro = bairro;
                        usuarios[index].enderecos[subIndex].cidade = cidade;
                        usuarios[index].enderecos[subIndex].estado = estado;
                        usuarios[index].enderecos[subIndex].pais = pais;
                      });
        }        
      }
      this.usuarios = usuarios;
    });
  }

  add() {
    const modal = this.modalService.open(ClienteFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
  }

  editar(usuario: Usuario) {
    const modal = this.modalService.open(ClienteFormComponent);   
    modal.result.then(this.handleModalForm.bind(this),
                      this.handleModalForm.bind(this))
    modal.componentInstance.insercao = false;
    modal.componentInstance.usuario = usuario;
  }

  handleModalForm(response) {
    
  }

}
